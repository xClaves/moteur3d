#pragma once

#include <GL/glew.h>
#include <string>

class Shader
{
    public:
        Shader(const GLchar* vertexPath, const GLchar* fragmentPath);
        Shader(const GLchar * vertexPath, const GLchar * geometryPath, const GLchar *fragmentPath);
        
        void bind();
    public:
        GLuint Program;
};