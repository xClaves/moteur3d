#include "Scene.h"
#include "SSAO.h"
#include "Shader.h"
#include "Camera.h"
#include "Window.h"

int main()
{
    const GLuint SCR_WIDTH = 1280, SCR_HEIGHT = 720;
    
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    Window window(SCR_WIDTH, SCR_HEIGHT);
    Camera &camera = window.getCamera();

    Shader lightShader("../resources/shaders/ssao/quadVertex.glsl", "../resources/shaders/ssao/lightFragment.glsl");

    lightShader.bind();
    glUniform1i(glGetUniformLocation(lightShader.Program, "gPosition"), 0);
    glUniform1i(glGetUniformLocation(lightShader.Program, "gNormal"), 1);
    glUniform1i(glGetUniformLocation(lightShader.Program, "gAlbedo"), 2);
    glUniform1i(glGetUniformLocation(lightShader.Program, "ssao"), 3);

    glm::vec3 lightPosition = glm::vec3(2.0, 4.0, -2.0);
    glm::vec3 lightColor = glm::vec3(0.2, 0.2, 0.6);

    GBuffer gbuffer(SCR_WIDTH, SCR_HEIGHT);
    SSAO ssao(&gbuffer);
    
    Scene scene;

    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

    while(window.run())
    {
        glfwPollEvents();
    
        glm::mat4 view = camera.GetViewMatrix();
        glm::mat4 projection = camera.GetProjectionMatrix(window.getAspect());
        
        // render the scene into multi-render-to-texture
        gbuffer.bind(view, projection);
        scene.render(&gbuffer);
        gbuffer.unbind();

        // ssao post-processing
        ssao.render(view, projection);

        // render the final output with fullscreen quad
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        lightShader.bind();
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gbuffer.getFBPosition());
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, gbuffer.getFBNormal());
        glActiveTexture(GL_TEXTURE2);
        glBindTexture(GL_TEXTURE_2D, gbuffer.getFBAlbedo());
        glActiveTexture(GL_TEXTURE3);
        glBindTexture(GL_TEXTURE_2D, ssao.getFBBlured());
        glm::vec3 lightPosView = glm::vec3(camera.GetViewMatrix() * glm::vec4(lightPosition, 1.0));
        glUniform3fv(glGetUniformLocation(lightShader.Program, "light.Position"), 1, &lightPosView[0]);
        glUniform3fv(glGetUniformLocation(lightShader.Program, "light.Color"), 1, &lightColor[0]);
        const GLfloat constant = 1.0;
        const GLfloat linear = 0.09;
        const GLfloat quadratic = 0.023;
        glUniform1f(glGetUniformLocation(lightShader.Program, "light.Constant"), constant);
        glUniform1f(glGetUniformLocation(lightShader.Program, "light.Linear"), linear);
        glUniform1f(glGetUniformLocation(lightShader.Program, "light.Quadratic"), quadratic);
        glUniform1i(glGetUniformLocation(lightShader.Program, "draw_mode"), window.draw_mode);
        gbuffer.render_quad();
        
        window.swap();
    }
    
    glfwTerminate();
    return 0;
}