#pragma once

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "Shader.h"

class GBuffer
{
    public:
        GBuffer(int width, int height);
        ~GBuffer();

        void bind(glm::mat4 view, glm::mat4 projection);
        void render_quad();
        void unbind();
        
        GLuint getFBPosition()  const { return gPosition; }
        GLuint getFBNormal()    const { return gNormal; }
        GLuint getFBAlbedo()    const { return gAlbedo; }
        
        GLuint getFrameBuffer() const { return gBuffer; }
        
        int getWidth()  const { return width; }
        int getHeight() const { return height; }
        
        Shader& getScreenShader() { return screenShader; }
    protected:
        GLuint gBuffer;
        GLuint gPosition, gNormal, gAlbedo;
        Shader screenShader;
        // fullscreen quad
        GLuint quadVAO, quadVBO;
    private:
        int width, height;
};
