#pragma once

#include "GBuffer.h"
#include "Model.h"

class Scene
{
    public:
        Scene();
        ~Scene();

        void render(GBuffer*);
    protected:
        void render_cube();
    private:
        Model cyborg;
        GLuint woodTexture;
        GLuint cubeVAO;
        GLuint cubeVBO;
};
