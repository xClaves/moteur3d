#pragma once

#include <vector>
#include <glm/glm.hpp>
#include <SOIL/SOIL.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include "Mesh.h"

GLuint TextureFromFile(std::string filename);

class Model
{
    public:
        Model(std::string path);

        void draw(const Shader &shader);

    protected:
        std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);
    private:
        std::vector<Mesh> meshes;
        std::string directory;
        std::vector<Texture> textures_loaded;
        
        void processNode(aiNode* node, const aiScene* scene);
        Mesh processMesh(aiMesh* mesh, const aiScene* scene);
};
